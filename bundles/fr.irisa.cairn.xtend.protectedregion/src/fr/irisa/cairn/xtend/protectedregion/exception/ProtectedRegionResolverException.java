package fr.irisa.cairn.xtend.protectedregion.exception;

@SuppressWarnings("serial")
public abstract class ProtectedRegionResolverException extends Throwable {
	public final int lineNum;
	
	public ProtectedRegionResolverException(int lineNum) {
		this.lineNum = lineNum;
	}
}