package fr.irisa.cairn.xtend.protectedregion.exception;

@SuppressWarnings("serial")
public class DuplicateRegion extends ProtectedRegionResolverException {
	
	public final String id;

	public DuplicateRegion(int lineNum, String id) {
		super(lineNum);
		this.id = id;
	}
	
	@Override
	public String getMessage() {
		return String.format("Duplicate protected region id (%s) at line %d", id, lineNum);
	}
	
}