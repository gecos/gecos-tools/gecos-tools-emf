package fr.irisa.cairn.xtend.protectedregion.exception;

@SuppressWarnings("serial")
public class UnmatchedRegionEnd extends ProtectedRegionResolverException {
	public final int regionStart;
	
	public UnmatchedRegionEnd(int lineNum, int regionStart) {
		super(lineNum);
		this.regionStart = regionStart;
	}

	@Override
	public String getMessage() {
		return String.format("Umatached protected region end from %d to %d", regionStart, lineNum);
	}
}