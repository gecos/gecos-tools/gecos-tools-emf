package fr.irisa.cairn.xtend.protectedregion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irisa.cairn.xtend.protectedregion.exception.DuplicateRegion;
import fr.irisa.cairn.xtend.protectedregion.exception.InputStreamError;
import fr.irisa.cairn.xtend.protectedregion.exception.NestedRegion;
import fr.irisa.cairn.xtend.protectedregion.exception.ProtectedRegionResolverException;
import fr.irisa.cairn.xtend.protectedregion.exception.UnmatchedRegionEnd;

/**
 * Handles protected regions on a per file basis. It is much less general than the original protected region in Xpand.
 * 
 * The resolver searches for protected region that start with:
 * PROTECTED REGION ID(unique_id) ENABLED START
 * and end with:
 * PROTECTED REGION END
 * 
 * These two signatures should be in comments of the target language.
 * It is also assumed that there is no other important text within the same line.
 * For example, if you have some text after 'END' of the ending signature, this text will also be protected.
 * 
 * You may replaced ENABLED by DISABLED to have a protected region that do not preserve the text.
 * This is useful to force the code generator to replace the body when desired.
 * 
 * The unique_id must not contain '(' or ')' or spaces/tabs. It is required to be unique within the file.
 * 
 * @author tyuki
 *
 */
public class ProtectedRegionResolver {
	

		protected static final Pattern start = Pattern.compile(".+PROTECTED\\s+REGION\\s+ID\\s*\\(\\s*([^\\(\\)\\s]+)\\s*\\)\\s+(ENABLED|DISABLED)\\s+START.+");
		protected static final Pattern end   = Pattern.compile(".+PROTECTED\\s+REGION\\s+END.+");

		protected final InputStream original;
		protected final InputStream generated;


		public static CharSequence resolve(InputStream orig, InputStream generated) throws ProtectedRegionResolverException {
			return (new ProtectedRegionResolver(orig, generated)).resolve();
		}
		
		protected ProtectedRegionResolver(InputStream original, InputStream generated) {
			this.original  = original;
			this.generated = generated;
		}
		
		protected CharSequence resolve() throws ProtectedRegionResolverException {
			try {
				BufferedReader brOrig = new BufferedReader( new InputStreamReader(original));
				BufferedReader brNew = new BufferedReader( new InputStreamReader(generated));

				ProtectedRegionCollector collector = (new ProtectedRegionCollector(brOrig));
				collector.scan();
				ProtectedRegionReplacer replacer = new ProtectedRegionReplacer(brNew, collector.regionToText);
				replacer.scan();
				
				return replacer.replacedText;
			} catch (IOException ioe) {
				throw new InputStreamError(ioe);
			}
		}
		
		protected abstract class ProtectedRegionScanner {
			
			protected final BufferedReader stream;
			
			public ProtectedRegionScanner(BufferedReader stream) {
				this.stream = stream;
			}
			
			protected void scan() throws IOException, ProtectedRegionResolverException {
				Set<String> previousIDs = new TreeSet<String>();
				
				String currentRegionID = null;
				int lineNum = 0;
				int regionStartLine = -1;
				boolean currentRegionEnabled = false;
				//find all enabled regions
				while (stream.ready()) {
					lineNum++;
					
					String line = stream.readLine();
					
					//region start
					Matcher startMatcher = start.matcher(line);
					if (startMatcher.matches()) {
						String id = startMatcher.group(1);
						currentRegionEnabled = startMatcher.group(2).contentEquals("ENABLED");
						if (previousIDs.contains(id)) {
							throw new DuplicateRegion(lineNum, id);
						}
						if (currentRegionID != null) {
							throw new NestedRegion(lineNum, currentRegionID, id);
						}
						currentRegionID = id;
						regionStartLine = lineNum;
						previousIDs.add(id);
						startRegion(lineNum, id, currentRegionEnabled, line);
						continue;
					}
					
					//region end
					Matcher endMatcher = end.matcher(line);
					if (endMatcher.matches()) {
						if (currentRegionID == null) {
							throw new UnmatchedRegionEnd(lineNum, regionStartLine);
						}
						
						//register
						endRegion(lineNum, currentRegionID, currentRegionEnabled, line);
						
						currentRegionID = null;
						regionStartLine = -1;
						currentRegionEnabled = false;
						continue;
					}
					
					processLine(lineNum, currentRegionID, currentRegionEnabled, line);
				}
			}

			protected abstract void startRegion(int lineNum, String prID, boolean enabled, String lineStr);
			protected abstract void processLine(int lineNum, String prID, boolean enabled, String lineStr);
			protected abstract void endRegion(int lineNum, String prID, boolean enabled, String lineStr);
		}
		
		protected class ProtectedRegionCollector extends ProtectedRegionScanner {
			
			protected Map<String, String> regionToText = new TreeMap<String, String>();
			
			private StringBuffer currentRegionText = null;

			public ProtectedRegionCollector(BufferedReader stream) {
				super(stream);
			}

			@Override
			protected void startRegion(int lineNum, String prID, boolean enabled, String lineStr) {
				currentRegionText = new StringBuffer(lineStr);
				currentRegionText.append("\n");
			}

			@Override
			protected void processLine(int lineNum, String prID, boolean enabled, String lineStr) {
				if (prID != null) {
					currentRegionText.append(lineStr);
					currentRegionText.append("\n");
				}
				
			}

			@Override
			protected void endRegion(int lineNum, String prID, boolean enabled, String lineStr) {
				currentRegionText.append(lineStr);
				currentRegionText.append("\n");
				if (enabled) {
					regionToText.put(prID, currentRegionText.toString());
				}
				currentRegionText = null;
			}
		}
		
		protected class ProtectedRegionReplacer extends ProtectedRegionScanner {
			protected final Map<String, String> replacement;
			
			private StringBuffer replacedText = new StringBuffer();
			private boolean skip = false;
			
			public ProtectedRegionReplacer(BufferedReader target, Map<String, String> replacement) {
				super(target);
				this.replacement = replacement;
			}

			@Override
			protected void startRegion(int lineNum, String prID, boolean enabled, String lineStr) {
				if (replacement.containsKey(prID)) {
					skip = true;
					replacedText.append(replacement.get(prID));
				} else {
					replacedText.append(lineStr);
					replacedText.append("\n");
				}
			}

			@Override
			protected void processLine(int lineNum, String prID, boolean enabled, String lineStr) {
				if (!skip) {
					replacedText.append(lineStr);
					replacedText.append("\n");
				}
			}

			@Override
			protected void endRegion(int lineNum, String prID, boolean enabled, String lineStr) {
				if (!skip) {
					replacedText.append(lineStr);
					replacedText.append("\n");
				}
				skip = false;
			}
		}
}
