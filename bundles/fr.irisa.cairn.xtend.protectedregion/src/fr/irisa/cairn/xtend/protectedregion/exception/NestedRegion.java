package fr.irisa.cairn.xtend.protectedregion.exception;

@SuppressWarnings("serial")
public class NestedRegion extends ProtectedRegionResolverException {
	public final String idOuter;
	public final String idInner;
	
	public NestedRegion(int lineNum, String idOuter, String idInner) {
		super(lineNum);
		this.idOuter = idOuter;
		this.idInner = idInner;
	}

	@Override
	public String getMessage() {
		return String.format("Nested protected region id (%s in %s) at line %d", idInner, idOuter, lineNum);
	}
}