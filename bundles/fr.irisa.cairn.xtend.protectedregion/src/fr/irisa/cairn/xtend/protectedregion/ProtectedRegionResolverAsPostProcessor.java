package fr.irisa.cairn.xtend.protectedregion;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.xtext.generator.IFilePostProcessor;

import fr.irisa.cairn.xtend.protectedregion.exception.ProtectedRegionResolverException;

/**
 * Interface to ProtectedRegionResolver as IFilePostProcessor
 * This should be binded as the post processor of AbstractFileSystemAccess
 * 
 * @author tyuki
 *
 */
public class ProtectedRegionResolverAsPostProcessor implements IFilePostProcessor {

	@Override
	public CharSequence postProcess(URI fileURI, CharSequence content) {
		try {
			return ProtectedRegionResolver.resolve(URIConverter.INSTANCE.createInputStream(fileURI), new ByteArrayInputStream(content.toString().getBytes("UTF-8")));
		} catch (IOException ioe) {
			return content;
		} catch (ProtectedRegionResolverException e) {
			throw new RuntimeException(e + " in file: " + fileURI);
		}
	}

}
