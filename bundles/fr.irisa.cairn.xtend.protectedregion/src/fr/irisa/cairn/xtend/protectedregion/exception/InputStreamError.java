package fr.irisa.cairn.xtend.protectedregion.exception;

import java.io.IOException;

@SuppressWarnings("serial")
public class InputStreamError extends ProtectedRegionResolverException {

	public final IOException ioe;
	
	public InputStreamError(IOException ioe) {
		super(-1);
		this.ioe = ioe;
	}
	
	@Override
	public String getMessage() {
		return String.format("Error in input stream: %s", ioe.getMessage());
	}	
}