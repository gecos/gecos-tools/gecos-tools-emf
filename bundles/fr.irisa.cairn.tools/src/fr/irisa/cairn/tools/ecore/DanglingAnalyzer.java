/**
 * 
 */
package fr.irisa.cairn.tools.ecore;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * Analyze references which are dangling from a reference {@link EObject}.
 * 
 * @author antoine
 * 
 */
public class DanglingAnalyzer {
	private static int COLLECT = 1;
	private static int EXCEPTION = 2;
	private EObject reference;
	private List<EObject> danglingRefs;
	private int mode;
	
	
	public DanglingAnalyzer(EObject reference) {
		mode=EXCEPTION;
		this.reference = reference;
	}

	/**
	 * An object is dangling if isn't contained or if its container isn't in
	 * reference containment hierarchy.
	 * 
	 * @param object
	 * @return
	 */
	public boolean isDangling(EObject object) {
		boolean noContainer = object.eContainer() == null;
		boolean notSameRootContainer = !isInReferenceContainmentHierarchy(object);
		if(noContainer) {
			return true;
		}
		if( notSameRootContainer) {
			return true;
		}
		return false;
	}

	public boolean hasDangling(EObject object) throws DanglingException {
		mode=EXCEPTION;
		return searchDangling(object); 
	}
		/**
	 * Test if object has some dangling references. All contained references are
	 * recursively analyzed.
	 * 
	 * @param object
	 * @return true if object has no dangling reference
	 * @throws DanglingException
	 */
	private boolean searchDangling(EObject object) throws DanglingException {
		boolean d = false;

		Iterator<EObject> iterator = object.eContents().iterator();
		while (iterator.hasNext() && !d) {
			EObject c = iterator.next();
			d = searchDangling(c);
		}
		iterator = object.eCrossReferences().iterator();
		while (iterator.hasNext() && !d) {
			EObject c = iterator.next();
			if(c!=null) d = isDangling(c);
			if (d) {
				if(mode==COLLECT) {
					danglingRefs.add(c);
				} else if (mode==EXCEPTION){
					throw new DanglingException(c);
					
				}
			}
		}

		return d;
	}

	/**
	 * Test if object has some dangling references. All contained references are
	 * recursively analyzed.
	 * 
	 * @param object
	 * @return true if object has no dangling reference
	 * @throws DanglingException
	 */
	public List<EObject> getAllDanglingReferences(EObject object) {
		mode= COLLECT;
		danglingRefs = new ArrayList<EObject>();
		try {
			searchDangling(object);
		} catch (DanglingException e) {
			// That should never happen !
			throw new RuntimeException(e.getMessage());
		}
		return danglingRefs;
	}

	private boolean isInReferenceContainmentHierarchy(EObject o) {
		if(o != reference) {
			boolean contained = o.eContainer() != null;
			if (contained) {
				return isInReferenceContainmentHierarchy(o.eContainer());
			} else {
				return false;
			}
		} else {
			return true;
		}
			
	}

	@SuppressWarnings("serial")
	public static class DanglingException extends Exception {
		private EObject dangling;

		public DanglingException(EObject dangling) {
			super(dangling + " is dangling.");
			this.dangling = dangling;
		}

		public EObject getDangling() {
			return dangling;
		}
	}

}