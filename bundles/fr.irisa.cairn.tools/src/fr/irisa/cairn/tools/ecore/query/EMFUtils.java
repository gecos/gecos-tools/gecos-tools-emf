package fr.irisa.cairn.tools.ecore.query;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class EMFUtils {
	
//	private T eContainerTypeSelect(EObject current) {
//
//		while (current != null) {
//			if (eclass.isSuperTypeOf(current.eClass())) {
//				@SuppressWarnings("unchecked")
//				T current2 = (T) current;
//				return current2;
//			}
//			current = current.eContainer();
//		}
//		return null;
//	}
//
//	public EList<T> eListTypeSelect(EList<EObject> root) {
//		EList<T> res = new BasicEList<T>();
//		Iterator<EObject> iterator = root.iterator();
//		while (iterator.hasNext()) {
//			EObject obj = (EObject) iterator.next();
//			if (eclass.isSuperTypeOf(obj.eClass())) {
//				@SuppressWarnings("unchecked")
//				T obj2 = (T) obj;
//				res.add(obj2);
//			}
//		}
//		return res;
//	}
//
//	public EList<T> eAllContentTypeSelect(EObject root) {
//		EList<T> res = new BasicEList<T>();
//		TreeIterator<EObject> iterator = root.eAllContents();
//		while (iterator.hasNext()) {
//			EObject obj = (EObject) iterator.next();
//			if (eclass.isSuperTypeOf(obj.eClass())) {
//				@SuppressWarnings("unchecked")
//				T obj2 = (T) obj;
//				res.add(obj2);
//			}
//		}
//		return res;
//	}

	/**
	 * Find the first object which is an instance of a subtype of <code>c</code>
	 * in the eContainer chain. Returns null if none is found.
	 * 
	 * @param current
	 * @param eclass
	 * @return
	 */
	public static <T extends EObject> T eContainerTypeSelect(EObject current, Class<T> c) {
		while (current != null) {
			if (c.isInstance(current)) {
				@SuppressWarnings("unchecked")
				T current2 = (T) current;
				return current2;
			}
			current = current.eContainer();
		}
		return null;
	}


	/**
	 * Iterate over all the containment list of <code>root</code> and replace
	 * every occurrence of the cross reference <code>oldref</code> by the cross
	 * reference <code>newref</code>.
	 * 
	 * FIXME : untested
	 * 
	 * @param root
	 * @param oldref
	 * @param newref
	 */
	public static void replaceAllRefsInContainement(EObject root, EObject oldref, EObject newref) {
		//explore the entire containment tree
		TreeIterator<EObject> i = root.eAllContents();
		while (i.hasNext()) {
			EObject current = i.next();
			
			//for each object, explore all his cross references
			for (EObject crossref : current.eCrossReferences()) {
				if (crossref == oldref) {
					//when we find an occurence of the ref to replace, 
					
					for (EReference feature : current.eClass().getEAllReferences()) {
						if (feature.isMany()) {
							@SuppressWarnings("unchecked")
							EList<EObject> eGet = (EList<EObject>) current.eGet(feature);
							if (eGet.contains(oldref)) {
								int indexOf = eGet.indexOf(oldref);
								eGet.set(indexOf, newref);
							}
						} else {
							EObject ref = (EObject) current.eGet(feature);
							if (ref == oldref) {
								current.eSet(feature, newref);
							}
						}
					}
				}
			}
		}
	}


	/**
	 * Substitute <code>oldObj</code> by <code>newObj</code> in
	 * <code>oldObj.eContainer()</code>
	 * 
	 * @param oldObj
	 * @param newObj
	 */
	public static void substituteByNewObjectInContainer(EObject oldObj, EObject newObj) {
		EStructuralFeature eContainingFeature = oldObj.eContainingFeature();
		EObject container = oldObj.eContainer();
		replaceFeature(oldObj, newObj, eContainingFeature, container);
	}


	private static void replaceFeature(EObject oldObj, EObject newObj, EStructuralFeature eContainingFeature, EObject container) {
		if (eContainingFeature.isMany()) {
			@SuppressWarnings("unchecked")
			EList<EObject> eGet = (EList<EObject>) container.eGet(eContainingFeature);
			eGet.add(eGet.indexOf(oldObj), newObj);
			eGet.remove(oldObj);
		} else {
			container.eSet(eContainingFeature, newObj);
		}
	}

	/**
	 * Find the root container of the given <code>current</code> EObject.
	 * Returns <code>current</code> if it is not contained.
	 * 
	 * @param current
	 * @return
	 */
	public static EObject eRootContainer(EObject current) {
		while (current.eContainer() != null) {
			current = current.eContainer();
		}
		return current;
	}


	/**
	 * <p>
	 * Iterate over all the containment elements of <code>root</code> and return
	 * the list of elements instance of <code>c</code>. Do not iterate inside
	 * containment tree of found elements (prune iterator).
	 * </p>
	 * <p>
	 * Note: if ''root'' is an instance of ''c'', it will <b>not</b> be part of
	 * the resulting list.
	 * </p>
	 * 
	 * @param root
	 *            the root of the containment tree to query
	 * @param c
	 *            the class to look for
	 * @return
	 */
	public static  <T extends EObject> EList<T>  eAllContentsFirstInstancesOf(EObject root, Class<T> c) {
		return eAllContentsInstancesOf(root, c,true);
	}
	
	/**
	 * <p>
	 * Iterate over all the containment elements of <code>root</code> and return
	 * the list of elements instance of <code>c</code>. Also iterate inside
	 * containment tree of found elements.
	 * </p>
	 * <p>
	 * Note: if ''root'' is an instance of ''c'', it will <b>not</b> be part of
	 * the resulting list.
	 * </p>
	 * <br/>
	 * 
	 * @param root
	 *            the root of the containment tree to query
	 * @param c
	 *            the class to look for
	 * @return
	 */
	public static <T extends EObject> EList<T> eAllContentsInstancesOf(EObject root, Class<T> c) {
		return eAllContentsInstancesOf(root, c,false);
	}
	
	private static  <T extends EObject> EList<T>  eAllContentsInstancesOf(EObject root, Class<T> c, boolean prune) {
		/* FIXME : Because of a hack in the containment for ComponentInstructions, reflexive 
		 * Query may gives duplicate results in some cases. We hence use an ordered 
		 * Set to collect the results and return a list 
		 */
		LinkedHashSet<T> res = new LinkedHashSet<T>();
		TreeIterator<EObject> iterator = root.eAllContents();
		while (iterator.hasNext()) {
			EObject obj = iterator.next();
			if (c.isAssignableFrom(obj.getClass())) {
				@SuppressWarnings("unchecked")
				T t = (T)obj;
				res.add(t);
				if (prune)
					iterator.prune();
			}
		}
		return ECollections.newBasicEList(res);
	}
	
	/**
	 * Iterate over all the containment elements of <code>roots</code> and
	 * return the list of elements instance of <code>c</code>. Also iterate
	 * inside containment tree of found elements.
	 * 
	 * @param roots
	 * @param eclass
	 * @return
	 */
	public static <T extends EObject> EList<T> eAllListContentsInstancesOf(List<EObject> roots, Class<T> c) {
		LinkedHashSet<T> res = new LinkedHashSet<T>();
		for (EObject eObject : roots) {
			res.addAll(eAllContentsInstancesOf(eObject, c));
		}
		return ECollections.newBasicEList(res);
	}


	/**
	 * 
	 * <p>
	 * Iterate over all the containment elements of <code>root</code> and return
	 * the list of elements instance of <code>c</code>. When the iterator
	 * encounters an object that is not instance of one of the class in
	 * <code>paths</code>, it prunes the branch.
	 * </p>
	 * <p>
	 * Note: if ''root'' is an instance of ''c'', it will <b>not</b> be part of
	 * the resulting list.
	 * </p>
	 * <br/>
	 * 
	 * @param root
	 *            the root of the containment tree to query
	 * @param c
	 *            the class to look for
	 * @param paths
	 *            the list of classes to look into
	 * @return
	 */
	public static <T extends EObject> EList<T> searchInstancesOfInPath(EObject root, Class<T> c, Class<?>... paths) {
		LinkedHashSet<T> res = new LinkedHashSet<T>();
		List<Class<?>> classes = Arrays.asList(paths);
		TreeIterator<EObject> iterator = root.eAllContents();
		while (iterator.hasNext()) {
			EObject obj = iterator.next();
			if (c.isAssignableFrom(obj.getClass())) {
				@SuppressWarnings("unchecked")
				T t = (T)obj;
				res.add(t);
			}
			if(!classes.contains(obj.getClass())) {
				iterator.prune();
			}
		}
		return ECollections.newBasicEList(res);
	}  



	public static <T extends EObject> EList<T> getAllReferencesOfType(EObject o, Class<T> c){
		LinkedHashSet<T> res = new LinkedHashSet<T>();
		if (o!=null) {
			for (EReference r : o.eClass().getEAllReferences()) {
				Object eGet = o.eGet(r);
				if (eGet!=null && c.isAssignableFrom(eGet.getClass())) {
					@SuppressWarnings("unchecked")
					T eGet2 = (T) eGet;
					res.add(eGet2);
				}
				if(r.isContainment()){
					if(r.getUpperBound()==-1){
						@SuppressWarnings("unchecked")
						List<EObject> eGet2 = (List<EObject>)eGet;
						for(EObject object: eGet2){
							res.addAll(getAllReferencesOfType(object, c));
						}
					}else
						res.addAll(getAllReferencesOfType((EObject) eGet, c));
				}
			}
		}
		return ECollections.newBasicEList(res);
	}
	
	public static Set<EObject> getAllReferencesOf(EObject eObjectToLookForReference, EObject eObjectWhereToLookFor) {
		Collection<Setting> settings = EcoreUtil.UsageCrossReferencer.find(eObjectToLookForReference, eObjectWhereToLookFor);
		LinkedHashSet<EObject> res = new LinkedHashSet<EObject>();
		for (Setting setting : settings)
			res.add(setting.getEObject());
		return res;
	}
}
