package fr.irisa.cairn.tools.ecore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

public class EMFModelPrinter {
	private EObject e;
	private Map<EObject, String> map;
	private Map<EObject, Integer> rankMap;
	private int i;
	private PrintStream out;
	
//	private String connector = "->";
//	private String containStyle = "[color=red]";
//	private String refStyle = "[style=dotted]";
//	private String rootStyle = "style=filled, fillcolor=red";
	private int depth;
	
	
	public EMFModelPrinter(EObject e, String path) {
		this.e = e;
		this.map = new HashMap<EObject, String>();
		this.rankMap = new HashMap<EObject, Integer>();
		this.i = 0;
		this.depth=0;
		File sortie = new File(path + ".dot");
		try {
			this.out =  new PrintStream(new FileOutputStream(sortie));
		} catch(IOException ioe) {
			throw new RuntimeException("Can't create the output file " + path + ".dot");
		}
	}
	
	public void compute() {
		System.out.println("	--- Model Dot Building");
		mapVisit(e);
		print();		
	}
	
	private boolean isCluster(EObject ve) {
		return (ve.eContents().size()>0);
	}
	private void mapVisit(EObject ve) {
		if (ve != null) {
			if(!isCluster(ve)) {
				map.put(ve, "n"+i);
			} else {
				map.put(ve, "cluster_"+i);
			}
			rankMap.put(ve, depth);
			i++;
			for (EObject content : ve.eContents()) {
				depth++;
				if (!map.containsKey(content)) {
					mapVisit(content);
				} else {
					rankMap.put(content, depth);
				}
				depth--;
			}
			for (EObject crossReference : ve.eCrossReferences()) {
				
				if (!map.containsKey(crossReference)) {
					mapVisit(crossReference);
				}
			}
			if (!map.containsKey(ve.eContainer())) {
				mapVisit(ve.eContainer());
			}
		}
	}
		
	
	private String clusterVisit(EObject ve) {
		
		String tmp="";
		if (ve != null ) {
			String simpleName = ve.getClass().getSimpleName().replace("Impl","");
			
			String label="\""+simpleName+"\"";
			if (ve.eContents().size()!=0) {
				tmp = "subgraph "+map.get(ve)+" {\n label="+label+"\n";
				for (EObject obj : ve.eContents()) {
					tmp+=clusterVisit(obj);
				}
				tmp+="\n}\n";
			} else {
				tmp = ""+map.get(ve)+" [label="+label+"]\n";
			}
//			for (EObject crossReference : ve.eCrossReferences()) {
//				if (!map.containsKey(crossReference)) {
//					throw new RuntimeException("Error");
//				} else {
//					tmp+= map.get(ve)+"->"+map.get(crossReference)+"\n";
//				}
//			} 
		}
		return tmp;
	}

	private String nonContainedVisit() {
		String tmp="";
		for (EObject key : map.keySet() ) {
			if (key != null) {
				for (EObject crossReference : key.eCrossReferences()) {
					if (crossReference!=null && crossReference.eContainer()==null) {
						tmp += clusterVisit(crossReference);
					}
				} 
			}
		}
		return tmp;
	}

	private String crossRefVisit() {
		String tmp="";
		for (EObject key : map.keySet() ) {
			if (key != null) {
				for (EObject crossReference : key.eCrossReferences()) {
					if (crossReference!=null)
						if (!map.containsKey(crossReference)) {
							throw new RuntimeException("Error");
						} else {
							tmp+= map.get(key)+"->"+map.get(crossReference)+"\n";
						}
				} 
			}
		}
		return tmp;
	}

	private void print() {
		out.println("digraph G {");
		out.println(clusterVisit(e));
		out.println(nonContainedVisit());
		out.println(crossRefVisit());
		out.println("}");
	}
}
